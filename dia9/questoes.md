## Dia9

### Questão 1
Nosso gerente precisa reporta para o nosso diretor, quais as namespaces que nós temos hoje em produção.
Salvar a lista de namespaces no arquivo /tmp/giropops-k8s.txt

### Resposta 1

<details>
  <summary><b>Resposta 1</b> <em>(clique para ver a resposta)</em></summary>

Para gerar a lista de todos os namespaces disponiveis e já adiciona-la no arquivo indicado.

```bash
kubectl get ns --no-headers -o custom-columns=":metadata.name" > /tmp/giropops-k8s.txt
```


</details>



---


### Questão 2
Precisamos criar um pod utilizando a imagem do NGINX na versão 1.21.4.
O Pod deverá ser criado no namespace web-1 e o container deverá se chamar meu-container-web.
O nosso gerente pediu para que seja criado um script que retorne o status desse Pod que iremos criar.
O nome do script é /tmp/script-do-gerente-toskao.sh

<details>
  <summary><b>Resposta 1</b> <em>(clique para ver a resposta)</em></summary>


```bash

```

Criar um Dry-run para obter o código YAML para criar um Pod com nome personalizado.
Pois não é possível criar um Pod com nome personalizado via linha de comando.

```bash
kubectl run web --image nginx:1.21.4 -n web-1 --dry-run=client -o yaml > pod.yaml
```

Agora edite o arquivo e adicione o nome do container conforme solicitado:

```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: web
  name: web
  namespace: web-1
spec:
  containers:
  - image: nginx:1.21.4
    name: meu-container-web
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```

```bash
kubectl create ns web-1
kubectl create -f pod.yaml
kubectl get pods -n web-1
```

Criando script que filtra pelo nome do Pod e o status dele:

```bash
echo 'kubectl get pods -n web-1 --no-headers -o custom-columns=":status.phase,:metadata.name"' > /tmp/script-do-gerente-toskao.sh
chmod +x /tmp/script-do-gerente-toskao.sh
```

Verificando os logs para ver se tudo está funcionando

```bash
kubectl logs -f meu-pod container-1
kubectl logs -f meu-pod container-2
kubectl logs -f meu-pod container-3
kubectl exec -ti meu-pod -c container-1 -- bash
```

</details>


### Questão 3

Criamos o pod do Nginx, parabéns! 

- TASK-1: Portanto, agora precisamos mudar a versão do Nginx para a versão 1.18.0, pois o
nosso gerente viu um artigo no Medium e disse que agora temos que usar essa
versão e ponto.

<details>
  <summary><b>Resposta TASK-1</b> <em>(clique para ver a resposta)</em></summary>

```bash
kubectl edit pods -n web-1 web
```
</details>

- TASK-2: Precisamos criar um deployment no lugar do nosso pod do Nginx

<details>
  <summary><b>Resposta TASK-2</b> <em>(clique para ver a resposta)</em></summary>

```bash
kubectl create deployment web --image nginx:1.20.2 --dry-run=client -o yaml > deployment.yaml
```

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: web
  name: web
  namespace: web-1
spec:
  replicas: 1
  selector:
    matchLabels:
      app: web
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: web
    spec:
      containers:
      - image: nginx:1.20.2
        name: meu-container-web
        resources: {}
status: {}
```
</details>

- TASK-3: Precisamos utilizar o Nginx com a imagem do Alpine, pq o gerente leu um outro artigo no Medium.

<details>
  <summary><b>Resposta TASK-3</b> <em>(clique para ver a resposta)</em></summary>

```bash
kubectl edit deployments.apps -n web-1 web
kubectl edit deployment -n web-1 web
```
</details>

- TASK-4: Precisamos realizar o rollback do nosso deployment web

<details>
  <summary><b>Resposta TASK-4</b> <em>(clique para ver a resposta)</em></summary>

```bash
kubectl rollout history deployment -n web-1 web
kubectl rollout history deployment -n web-1 web --revision=1
kubectl rollout history deployment -n web-1 web --revision=2
kubectl rollout undo deployment -n web-1 web --to-revision=1
```

</details>