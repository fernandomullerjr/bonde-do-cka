# BondeDoCKA

Repositório com os materiais referentes as aulas do bonde do CKA, visando a obtenção da certificação inicial do Kubernetes.

- [Questões do Dia1](dia1/questoes.md)
- [Questões do Dia2](dia2/questoes.md)
- [Questões do Dia3](dia3/questoes.md)
- [Questões do Dia4](dia4/questoes.md)
- [Questões do Dia5](dia5/questoes.md)
- [Questões do Dia6](dia6/questoes.md)
- [Questões do Dia7](dia7/questoes.md)
- [Questões do Dia8](dia8/questoes.md)
- [Questões do Dia9](dia9/questoes.md)