## DAY4

### Questão 1
Precisamos subir um container em um node master.
Esse Container tem que estar rodando a imagem do NGINX, o nome do pod é pod-web e o Container é container-web.
Sua namespace será a catota.

### Resposta 1

```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: pod-web
  name: pod-web
  namespace: catota
spec:
  containers:
  - image: nginx
    name: container-web
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Always
  tolerations:
  - effect: NoSchedule
    operator: Equal
    key: node-role.kubernetes.io/master
  nodeSelector:
    node-role.kubernetes.io/master: ""  
status: {}

```

```bash
kubectl create namespace catota
kubectl create -f pod-web.yaml
```



```yaml


```




### Questão 2
Precisamos de algumas informações do nosso cluster e dos pods que lá estão.
Portanto, precisamos do seguinte:
- Adicione a todos os pods do cluster por ordem de criação, dentro do arquivo
    /opt/pods.txt
- Remova um pod do weave e adicione os eventos no arquivo
    /opt/eventos.txt
- Liste todos os pods que estão em execução no node xyz e adicione
### Resposta 2



```bash
kubectl get pods --sort-by=.metadata.creationTimestamp -A > /opt/pods.txt
kubectl get events --all-namespaces --sort-by=.metadata.creationTimestamp > /opt/eventos.txt
kubectl get pods --all-namespaces --field-selector spec.nodeName=giropops-control-plane > 
```



```bash

```



```bash

```

### Questão 3



### Resposta 3



```bash

```

