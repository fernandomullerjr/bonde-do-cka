
# BondeDoCKA

## DIA 1

### Questão 1

- Criar um Pod utilizando a imagem do Nginx 1.18.0, com o nome giropops, no namespace strigus.

Nesse caso, temos duas formas.
A primeira, utilizando somente a linha de comando. E a segunda forma usando o comando run, parametro dry-run e jogando a configuração
para um arquivo yaml.
-PRIMEIRA FORMA:

```bash
kubectl run giropops --image=nginx:1.18.0 --port=80 --namespace=strigus
```

A segunda, e a mais recomendada. Eu acho ela mais recomendada pelo fato de você poder analisar com mais tranquilidade o que você está criando. Usando o comando RUN, usando o DRY-RUN e jogando o output para um arquivo YAML. 
Depois disto, usar o comando CREATE para criar o pod baseado no arquivo YAML.
-SEGUNDA FORMA:

```bash
kubectl run giropops --image=nginx:1.18.0 --port=80 --namespace=strigus --dry-run=client -o yaml > pod.yaml
kubectl create -f pod.yaml
```


---


### Questão 2

Aumentar a quantidade de réplicas do deployment girus, que está utilizando a imagem do nginx 1.18.0, para 3 réplicas.
O deployment está no namespace strigus.
Existem 3 formas, exemplificadas abaixo:

```bash
kubectl scale deployment -n strigus girus --replicas 3
```

```bash
kubectl create deployment girus --image=nginx:1.18.0 --port=80 --namespace=strigus --replicas 3 --dry-run=client -o yaml > deployment2.yaml
kubectl apply -f deployment2.yaml
```

```bash
kubectl edit deploy -n strigus girus
```


---


### Questão 3

Precisamos atualizar a versão do Nginx do Pod giropops. 
Ele está na versão 1.18.0 e precisamos atualizar para a versão 1.21.1.

```bash
kubectl edit pod -n strigus giropops
```

```bash
kubectl set image pod <nome-do-pod> -n strigus <nome-do-container>=nginx:1.20.1
kubectl set image pod giropops -n strigus giropops=nginx:1.20.1
```

```bash
kubectl get pods -n strigus giropops -o yaml > saida-do-get-pods.yaml
# Lembre-se de remover as sujeiras, deixando somente o necessário
kubectl apply -f saida-do-get-pods.yaml
```