## Dia6

### Questão 1
O nosso gerente observou no Lens que um dos nossos nodes não está bem.
Temos algum problema com o nosso cluster e precisamos resolver agora.


### Resposta 1

<details>
  <summary><b>Resposta 1</b> <em>(clique para ver a resposta)</em></summary>

```bash
kubectl get nodes
ssh node_com_problema
ps -ef | grep kubelet
docker ps
systemctl status kubelet
journalctl -u kubelet
whereis kubelet
vim /etc/systemd/system/kubelet.service.d/10-kubeadm.conf #arrumar o binario do kubelet
systemctl daemon-reload
systemctl restart kubelet
systemctl status kubelet
systemctl edit --full kubelet
```
</details>



---


### Questão 2



<details>
  <summary><b>Resposta 2</b> <em>(clique para ver a resposta)</em></summary>
  
```bash

```
</details>



