



########### Precisamos levantar algumas informações sobre o nosso cluster:






#############################################################################
- Quantos nodes são workers
0

É possível verificar com o comando:
kubectl get nodes
root@ubuntulab:/home/fernando# kubectl get nodes
NAME                     STATUS   ROLES                  AGE   VERSION
giropops-control-plane   Ready    control-plane,master   7d    v1.21.1
root@ubuntulab:/home/fernando#

Como só tem um nó e claramente como master, não temos um worker neste caso.






root@ubuntulab:/home/fernando# kubectl get pods -n kube-system
NAME                                             READY   STATUS    RESTARTS   AGE
coredns-558bd4d5db-n6nd7                         1/1     Running   12         7d
coredns-558bd4d5db-xh6gd                         1/1     Running   12         7d
etcd-giropops-control-plane                      1/1     Running   5          3d19h
kindnet-zdm7b                                    1/1     Running   12         7d
kube-apiserver-giropops-control-plane            1/1     Running   5          3d19h
kube-controller-manager-giropops-control-plane   1/1     Running   12         7d
kube-proxy-t2lp8                                 1/1     Running   12         7d
kube-scheduler-giropops-control-plane            1/1     Running   12         7d
root@ubuntulab:/home/fernando#
root@ubuntulab:/home/fernando#






########################################################################################
############ VERIFICANDO O CNI
kubectl get pods -n kube-system -o wide 

cd /etc/cni






kubectl get daemonsets -n kube-system

root@ubuntulab:/home/fernando# kubectl get daemonsets -n kube-system
NAME         DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR            AGE
kindnet      1         1         1       1            1           <none>                   7d
kube-proxy   1         1         1       1            1           kubernetes.io/os=linux   7d
root@ubuntulab:/home/fernando#

kubectl describe pods -n kube-system kindnet-zdm7b







############ init container
Ele sobe antes dos demais containers, para ir preparando o ambiente.




####################################################################################
####################################################################################
####################################################################################
####################################################################################
# VERIFICANDO DETALHES DO NODE:

kubectl describe nodes giropops-control-plane

root@ubuntulab:/home/fernando# kubectl describe nodes giropops-control-plane
Name:               giropops-control-plane
Roles:              control-plane,master
Labels:             beta.kubernetes.io/arch=amd64
                    beta.kubernetes.io/os=linux
                    kubernetes.io/arch=amd64
                    kubernetes.io/hostname=giropops-control-plane
                    kubernetes.io/os=linux
                    node-role.kubernetes.io/control-plane=
                    node-role.kubernetes.io/master=
                    node.kubernetes.io/exclude-from-external-load-balancers=
Annotations:        kubeadm.alpha.kubernetes.io/cri-socket: unix:///run/containerd/containerd.sock
                    node.alpha.kubernetes.io/ttl: 0
                    volumes.kubernetes.io/controller-managed-attach-detach: true
CreationTimestamp:  Fri, 17 Sep 2021 22:34:42 +0000
Taints:             <none>
Unschedulable:      false
Lease:
  HolderIdentity:  giropops-control-plane
  AcquireTime:     <unset>
  RenewTime:       Fri, 24 Sep 2021 23:00:10 +0000
Conditions:
  Type             Status  LastHeartbeatTime                 LastTransitionTime                Reason                       Message
  ----             ------  -----------------                 ------------------                ------                       -------
  MemoryPressure   False   Fri, 24 Sep 2021 22:55:19 +0000   Fri, 17 Sep 2021 22:34:32 +0000   KubeletHasSufficientMemory   kubelet has sufficient memory available
  DiskPressure     False   Fri, 24 Sep 2021 22:55:19 +0000   Fri, 17 Sep 2021 22:34:32 +0000   KubeletHasNoDiskPressure     kubelet has no disk pressure
  PIDPressure      False   Fri, 24 Sep 2021 22:55:19 +0000   Fri, 17 Sep 2021 22:34:32 +0000   KubeletHasSufficientPID      kubelet has sufficient PID available
  Ready            True    Fri, 24 Sep 2021 22:55:19 +0000   Mon, 20 Sep 2021 03:16:11 +0000   KubeletReady                 kubelet is posting ready status





############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################
############ VERIFICANDO O BLOCO CIDR UTILIZADO PELOS PODS DO NÓ:

kubectl describe nodes giropops-control-plane | grep CIDR

root@ubuntulab:/home/fernando# kubectl describe nodes giropops-control-plane | grep CIDR
PodCIDR:                      10.244.0.0/24
PodCIDRs:                     10.244.0.0/24
root@ubuntulab:/home/fernando#


root@ubuntulab:/home/fernando# kubectl cluster-info dump | head
{
    "kind": "NodeList",
    "apiVersion": "v1",
    "metadata": {
        "resourceVersion": "47877"
    },
    "items": [
        {
            "metadata": {
                "name": "giropops-control-plane",
root@ubuntulab:/home/fernando#

kubectl cluster-info dump | grep CIDR

root@ubuntulab:/home/fernando# kubectl cluster-info dump | grep CIDR
                "podCIDR": "10.244.0.0/24",
                "podCIDRs": [
I0924 22:50:47.301295       1 range_allocator.go:116] No Secondary Service CIDR provided. Skipping filtering out secondary service addresses.
I0924 22:50:47.502780       1 range_allocator.go:172] Starting range CIDR allocator
W0924 22:50:37.041471       1 server_others.go:512] detect-local-mode set to ClusterCIDR, but no IPv6 cluster CIDR defined, , defaulting to no-op detect-local for IPv6
root@ubuntulab:/home/fernando#



k get node -o jsonpath="{range .items[*]}{.metadata.name} {.spec.PodCIDR}"
k describe pods -n kube-system kube-apiserver-seul-cool-01 | grep -i cidr

kubectl get node -o jsonpath="{range.items[*]}{.metadata.name} {.spec.PodCIDR}"



kubectl describe pods -n kube-system kube-controller-manager-giropops-control-plane | grep -i CIDR

root@ubuntulab:/home/fernando# kubectl describe pods -n kube-system kube-controller-manager-giropops-control-plane | grep -i CIDR
      --allocate-node-cidrs=true
      --cluster-cidr=10.244.0.0/16
root@ubuntulab:/home/fernando#









############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################
### Verificando o DNS 

kubectl get pods -n kube-system | grep dns

root@ubuntulab:/home/fernando# kubectl get pods -n kube-system | grep dns
coredns-558bd4d5db-n6nd7                         1/1     Running   12         7d
coredns-558bd4d5db-xh6gd                         1/1     Running   12         7d
root@ubuntulab:/home/fernando#







############################################################################################################
############################################################################################################
############################################################################################################
############################################################################################################
### Criando os Containers

echo 'giropops strigus girus' > /tmp/toskaocat


kubectl apply -f pods-da-aula2.yml

root@ubuntulab:/home/fernando/bonde-do-cka/dia2# kubectl apply -f pods-da-aula2.yml
pod/init-demo created
root@ubuntulab:/home/fernando/bonde-do-cka/dia2#








##### TSHOOT  - Erros de CrashLoopBackOff

kubectl get pods -o wide

root@ubuntulab:/home/fernando/bonde-do-cka/dia2# kubectl get pods -o wide
NAME                     READY   STATUS             RESTARTS   AGE     IP           NODE                     NOMINATED NODE   READINESS GATES
giropops                 1/1     Running            16         7d      10.244.0.4   giropops-control-plane   <none>           <none>
girus-5dff565dfc-gg849   1/1     Running            12         7d      10.244.0.7   giropops-control-plane   <none>           <none>
girus-5dff565dfc-nczvk   1/1     Running            12         7d      10.244.0.2   giropops-control-plane   <none>           <none>
init-demo                2/3     CrashLoopBackOff   5          5m33s   10.244.0.8   giropops-control-plane   <none>           <none>
root@ubuntulab:/home/fernando/bonde-do-cka/dia2#


kubectl describe pod init-demo
kubectl describe pod init-demo
kubectl describe pod init-demo



kubectl logs -f init-demo container-1
kubectl logs -f init-demo container-2
kubectl logs -f init-demo container-3
kubectl logs -f init-demo container-1

kubectl apply -f pods-da-aula2.yml



kubectl delete pod init-demo

kubectl apply -f pods-da-aula2.yml

kubectl get pods
kubectl describe pod pods-questao-2-aula2

kubectl delete pod pods-questao-2-aula2
kubectl apply -f pods-da-aula2.yml

- O terceiro container tava sempre dando erro de backoff.
-Verificado que é devido ele morrer após subir.

https://stackoverflow.com/questions/31870222/how-can-i-keep-a-container-running-on-kubernetes

-Erro:
  Warning  BackOff    10s (x2 over 11s)  kubelet            Back-off restarting failed container
root@ubuntulab:/home/fernando/bonde-do-cka/dia2#

-SOLUÇÃO:
-Resolvido após ajustar o Container-3, adicionado no yaml:
    - name: container-3
      image: alpine
      command: ["sh", "-c", "tail -f /dev/null"]

kubectl apply -f pods-da-aula2.yml
kubectl get pods
kubectl describe pod containers-questao-2-aula2