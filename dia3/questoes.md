
# BondeDoCKA

## DIA 3

### Questão 1
Criar um pod estático utilizando a imagem do NGINX.

### Questão 1
Para criar um pod estático, você precisa adicionar o manifesto de criação do pod desejado, dentro de um diretório /etc/kubernetes/manifests, conforme abaixo:

```bash
cd /etc/kubernetes/manifests
kubectl run giropops --image=nginx -o yaml --dry-run=client > pod_estatico.yaml
vim pod-estatico.yaml
```

Adicione o conteúdo abaixo:

```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: giropops
  name: giropops
spec:
  containers:
  - image: nginx
    name: giropops
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```

---


### Questão 2
O nosso gerente está assustado, pois conversando com o gerente da nossa empresa, ficou sabendo que aconteceu uma indisponibilidade do Kubernetes de lá, por conta de certificados expirados.
Ele está maluco!
Ele quer que tenhamos a certeza de que nosso cluster não corre esse perigo, portanto, adicione no arquivo /tmp/meus-certificados.txt todos eles e suas datas de expiração.

```bash
 
```
 

### Resposta 2


```bash
 
```

```bash
 
```

---


### Questão 3
Vimos que precisamos atualizar o nosso cluster imediatamente, sem trazer nenhuma indisponibilidade para o ambiente.
Como devemos proceder?

### Resposta 3
Podemos utilizar o comando kubeadm certs para visualizar as datas corretas e também para realizar sua renovação. Conforme estamos fazendo abaixo:

```bash
kubeadm certs renew all
```

Lembrando a importancia de realizar o procedimento em todos os nodes master.