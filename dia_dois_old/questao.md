
### Questão 1
- Criar um Pod utilizando a imagem do Nginx 1.18.0, com o nome giropops, no namespace 
Nesse caso, temos duas formas.
A primeira, utilizando somente a linha de comando:

-PRIMEIRA FORMA:

```bash
kubectl run giropops --image=nginx:1.18.0 --port=80 --namespace=strigus
```

-SEGUNDA FORMA:

```bash
kubectl run giropops --image=nginx:1.18.0 --port=80 --namespace=strigus --dry-run=client -o yaml > pod.yaml
kubectl create -f pod.yaml
```

### Questão 2
Aumentar a quantidade de réplicas do deployment girus, que está utilizando a imagem do nginx 1.18.0, para 3 réplicas.
O deployment está no namespace strigus.
Existem 3 formas, exemplificadas abaixo:

```bash
kubectl scale deployment -n strigus girus --replicas 3
```

```bash
kubectl create deployment girus --image=nginx:1.18.0 --port=80 --namespace=strigus --replicas 3 --dry-run=client -o yaml > deployment1.yaml
kubectl apply -f deployment1.yaml
```

```bash
kubectl edit deploy -n strigus girus
```

### Questão 3
Precisamos atualizar a versão do Nginx do Pod giropops. Ele está na versão 1.18.0 e precisamos atualizar para a versão 1.21.1.

```bash
kubectl edit pod -n strigus giropops
```

```bash
kubectl set image pod <nome-do-pod> -n strigus <nome-do-container>=nginx:1.20.1
kubectl set image pod giropops -n strigus giropops=nginx:1.20.1
```
