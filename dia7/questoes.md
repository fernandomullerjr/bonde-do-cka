## Dia7

### Questão 1
 Precisamos subir um Pod, facil não?
 Porém esse Pod somente poderá ficar disponível quando um determinado serviço ficar ativo/respondendo.

 O serviço deverá ser um simples NGINX.

 O Pod, nós teremos mais detalhes durante a resolução.


### Resposta 1

<details>
  <summary><b>Resposta 1</b> <em>(clique para ver a resposta)</em></summary>

```bash
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: my-nginx
  name: my-nginx
  namespace: strigus
spec:
  containers:
  - image: nginx:1.18.0
    name: my-nginx
    ports:
    - containerPort: 80
    resources: {}
    livenessProbe:
      exec:
        command:
          - 'true'
    readinessProbe:
      exec:
        command:
          - cat
          - /etc/hosts
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```

```bash
kubectl delete -f pod-que-o-pod-esta-esperando.yaml
kubectl create -f pod-que-o-pod-esta-esperando.yaml
kubectl expose pod my-nginx
```

```bash
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: giropops
  name: giropops
  namespace: strigus
spec:
  containers:
  - image: nginx:1.18.0
    name: giropops
    ports:
    - containerPort: 80
    resources: {}
    livenessProbe:
      exec:
        command:
          - 'true'
    readinessProbe:
      exec:
        command:
          - sh
          - -c
          - 'curl http://my-nginx:80'
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```

```bash
kubectl delete -f pod-que-espera-o-service.yaml
kubectl create -f pod-que-espera-o-service.yaml
```

</details>



---


### Questão 2



<details>
  <summary><b>Resposta 2</b> <em>(clique para ver a resposta)</em></summary>
  
```bash

```
</details>



